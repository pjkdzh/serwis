module edu.ib.bazaserwis {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens edu.ib.bazaserwis to javafx.fxml;
    exports edu.ib.bazaserwis;
}
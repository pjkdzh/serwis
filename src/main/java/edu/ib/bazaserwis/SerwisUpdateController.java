package edu.ib.bazaserwis;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

/**
 * Create a method that carries the message of the field,
 * takes the user to the designated window
 * and carries the message of the field,
 * sets the value of the field from the available options,
 * select the value of a field based on the corresponding login
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class SerwisUpdateController {

    private final ObservableList<String> status = FXCollections.observableArrayList("Rozpatrzone", "W trakcie", "Nierozpatrzone");
    private final ObservableList<String> decision = FXCollections.observableArrayList("", "Naprawa", "Nowe urzadzenie", "Odrzucono");
    private ObservableList<String> number = FXCollections.observableArrayList("");

        @FXML
        private ResourceBundle resources;

        @FXML
        private URL location;

        @FXML
        private ComboBox numberChoice;

        @FXML
        private ComboBox decisionChoice;

        @FXML
        private ComboBox statusChoice;


    private String loginField;
    /**
     * Carries the message of the field
     * @param message e
     * @throws SQLException e
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
        init(loginField);
    }

    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event vent
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
        void btnBackFromUpdateClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis.fxml"));
        Parent root = loader.load();
        SerwisController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 1400, 600).newWindow(event);
        }

    /**
     * sets the value of the field from the available options
     * @param event event
     * @throws SQLException exception
     */
        @FXML
        void decisionChoiceAction(ActionEvent event) throws SQLException {
            Statement statement = Main.databaseConnection().createStatement();
            statement.executeUpdate("update dla_serwisu_aktywne set decyzja = \"" + decisionChoice.getValue().toString() + "\" where numerZgloszenia = \"" + numberChoice.getValue() + "\";" );
        }
    /**
     * sets the value of the field from the available options
     * @param event e
     * @throws SQLException e
     */
        @FXML
        void statusChoiceAction(ActionEvent event) throws SQLException {
            Statement statement = Main.databaseConnection().createStatement();
            statement.executeUpdate("update dla_serwisu_aktywne set status = \"" + statusChoice.getValue().toString() + "\" where numerZgloszenia = \"" + numberChoice.getValue() + "\";" );

        }
    /**
     * select the value of a field based on the corresponding login
     * @param loginField login
     * @throws SQLException exception
     */
        @FXML
        void init(String loginField) throws SQLException {
            PreparedStatement selectZgloszenieAktywne = Main.databaseConnection().prepareStatement("select numerZgloszenia from dla_serwisu_aktywne where login = " + "\"" + loginField + "\"" + ";");
            ResultSet zgloszenieAktywne = selectZgloszenieAktywne.executeQuery();
            while (zgloszenieAktywne.next()) {
                String answerZgloszenie = zgloszenieAktywne.getString(1);
                number.addAll(answerZgloszenie);
            }
            statusChoice.setItems(status);
            decisionChoice.setItems(decision);
            numberChoice.setItems(number);
        }
    /**
     * Initializing fields used in the scene
     * @throws SQLException exception
     */
        @FXML
        void initialize() throws SQLException {
            assert numberChoice != null : "fx:id=\"numberChoice\" was not injected: check your FXML file 'serwis_update.fxml'.";
            assert decisionChoice != null : "fx:id=\"decisionChoice\" was not injected: check your FXML file 'serwis_update.fxml'.";
            assert statusChoice != null : "fx:id=\"statusChoice\" was not injected: check your FXML file 'serwis_update.fxml'.";


        }
    }



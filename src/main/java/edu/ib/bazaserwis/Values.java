package edu.ib.bazaserwis;

import java.util.ArrayList;

/**
 * Creates a class and
 * adds values to the list
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class Values {
    private String serwis;
    private String zgloszenie;
    private String data;
    private String status;
    private String decyzja;

    /**
     * Creates a class
     *
     * @param serwis s
     * @param zgloszenie z
     * @param data d
     * @param status s
     * @param decyzja d
     */
    public Values(String serwis, String zgloszenie, String data, String status, String decyzja) {
        this.serwis= serwis;
        this.zgloszenie = zgloszenie;
        this.data = data;
        this.status = status;
        this.decyzja = decyzja;
    }

    public String getSerwis() {
        return serwis;
    }

    public void setSerwis(String serwis) {
        this.serwis = serwis;
    }

    public String getZgloszenie() {
        return zgloszenie;
    }

    public void setZgloszenie(String zgloszenie) {
        this.zgloszenie = zgloszenie;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDecyzja() {
        return decyzja;
    }

    public void setDecyzja(String decyzja) {
        this.decyzja = decyzja;
    }

    /**
     * adds values to the list
     * @param serwis serwis
     * @param zgloszenie zgloszenia
     * @param data data
     * @param status status
     * @param decyzja decyzja
     * @return values
     */
    public static ArrayList<Values> getValues(String serwis, String zgloszenie, String data, String status, String decyzja ){
        ArrayList<Values> values = new ArrayList<>();
        values.add(new Values(serwis, zgloszenie, data, status, decyzja));
        return values;
    }
}

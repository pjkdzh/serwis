package edu.ib.bazaserwis;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Takes the user to the designated window
 * and initializing fields used in the scene
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class StartViewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnSerwis;

    @FXML
    private Button btnKlient;

    @FXML
    private ImageView imgFoto;
    /**
     * Takes the user to the designated window
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void btnSerwisOnClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main_serwis.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void btnKlientOnClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Initializing fields used in the scene
     */
    @FXML
    void initialize() {
        assert btnSerwis != null : "fx:id=\"btnSerwis\" was not injected: check your FXML file 'start_view.fxml'.";
        assert btnKlient != null : "fx:id=\"btnKlient\" was not injected: check your FXML file 'start_view.fxml'.";

    }
}

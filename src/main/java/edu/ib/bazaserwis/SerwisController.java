package edu.ib.bazaserwis;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Create a method that carries the message of the field,
 * takes the user to the designated window
 * carries the message of the text field,
 * takes the user to the designated window
 * using data from a view created in a database
 * and using them in the table in scene
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class SerwisController {

    private ObservableList<ValuesSerwis> list1 = FXCollections.observableArrayList();
    private ObservableList<ValuesSerwis> list2 = FXCollections.observableArrayList();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableColumn<ValuesSerwis, String> nazwaUrzadzeniaAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> typAktualne;

    @FXML
    private Button btnUpdate;

    @FXML
    private TableColumn<ValuesSerwis, String> nazwiskoZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> numerZgloszeniaAktualne;

    @FXML
    private TableView<ValuesSerwis> table2;

    @FXML
    private TableView<ValuesSerwis> table1;

    @FXML
    private TableColumn<ValuesSerwis, String> dataZgloszeniaAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> dataZakonczenia;

    @FXML
    private TableColumn<ValuesSerwis, String> statusZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> numerKlientaZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> numerZgloszeniaZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> typZakonczone;

    @FXML
    private Button btnBack;

    @FXML
    private TableColumn<ValuesSerwis, String> numerSeryjnnyZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> decyzjaAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> cenaAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> cenaZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> numerKlientaAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> statusAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> telefonAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> decyzjaZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> telefonZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> nazwaUrzadzeniaZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> nazwiskoAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> numerSeryjnyAktualne;

    @FXML
    private TableColumn<ValuesSerwis, String> emailZakonczone;

    @FXML
    private TableColumn<ValuesSerwis, String> emailAktualne;
    private String loginField;

    /**
     * Carries the message of the field
     * @param message message
     * @throws SQLException exception
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
        init(loginField);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void onClickedUpdate(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis_update.fxml"));
        Parent root = loader.load();
        SerwisUpdateController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void onClickedBack(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis_wybor.fxml"));
        Parent root = loader.load();
        SerwisWyborController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnOpisClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/opis.fxml"));
        Parent root = loader.load();
        OpisController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Using data from a view created in a database
     * and using them in the table in scene
     * @param loginField login
     * @throws SQLException exception
     */
    @FXML
    void init(String loginField) throws SQLException {
        PreparedStatement selectAllZakonczone = Main.databaseConnection().prepareStatement("select * from dla_serwisu_zakonczone where login = " + "\"" + loginField + "\"" + ";");
        ResultSet rsAllZakonczone = selectAllZakonczone.executeQuery();
        while (rsAllZakonczone.next()) {
            String answerNumerZgloszenia = rsAllZakonczone.getString(1);
            String answerDataZakonczenia = rsAllZakonczone.getString(5);
            String answerDecyzja = rsAllZakonczone.getString(3);
            String answerStatus = rsAllZakonczone.getString(4);
            String answerNumerSeryjny = rsAllZakonczone.getString(10);
            String answerNazwaUrzadzenia = rsAllZakonczone.getString(11);
            String answerTyp = rsAllZakonczone.getString(12);
            String answerCena = rsAllZakonczone.getString(13);
            String answerNumerKlienta = rsAllZakonczone.getString(6);
            String answerNazwisko = rsAllZakonczone.getString(7);
            String answerTelefon = rsAllZakonczone.getString(8);
            String answerEmail = rsAllZakonczone.getString(9);

            list2.addAll(ValuesSerwis.getValuesSerwis(answerNumerZgloszenia, answerDataZakonczenia, answerDecyzja, answerStatus, answerNumerSeryjny, answerNazwaUrzadzenia, answerTyp, answerCena, answerNumerKlienta, answerNazwisko, answerTelefon, answerEmail));
            numerZgloszeniaZakonczone.setCellValueFactory(new PropertyValueFactory<>("numerZgloszenia"));
            dataZakonczenia.setCellValueFactory(new PropertyValueFactory<>("data"));
            decyzjaZakonczone.setCellValueFactory(new PropertyValueFactory<>("Decyzja"));
            statusZakonczone.setCellValueFactory(new PropertyValueFactory<>("Status"));
            numerSeryjnnyZakonczone.setCellValueFactory(new PropertyValueFactory<>("numerSeryjny"));
            nazwaUrzadzeniaZakonczone.setCellValueFactory(new PropertyValueFactory<>("nazwaUrzadzenia"));
            typZakonczone.setCellValueFactory(new PropertyValueFactory<>("Typ"));
            cenaZakonczone.setCellValueFactory(new PropertyValueFactory<>("Cena"));
            numerKlientaZakonczone.setCellValueFactory(new PropertyValueFactory<>("numerKlienta"));
            nazwiskoZakonczone.setCellValueFactory(new PropertyValueFactory<>("imieNazwisko"));
            telefonZakonczone.setCellValueFactory(new PropertyValueFactory<>("numerTelefonu"));
            emailZakonczone.setCellValueFactory(new PropertyValueFactory<>("Email"));
            table2.setItems(list2);
        }
        PreparedStatement selectAllAktywne = Main.databaseConnection().prepareStatement("select * from dla_serwisu_aktywne where login = " + "\"" + loginField + "\"" + ";");
        ResultSet rsAllAktywne = selectAllAktywne.executeQuery();
        while (rsAllAktywne.next()) {
            String answerNumerZgloszenia = rsAllAktywne.getString(1);
            String answerDataZgloszenia = rsAllAktywne.getString(2);
            String answerDecyzja = rsAllAktywne.getString(3);
            String answerStatus = rsAllAktywne.getString(4);
            String answerNumerSeryjny = rsAllAktywne.getString(9);
            String answerNazwaUrzadzenia = rsAllAktywne.getString(10);
            String answerTyp = rsAllAktywne.getString(11);
            String answerCena = rsAllAktywne.getString(12);
            String answerNumerKlienta = rsAllAktywne.getString(5);
            String answerNazwisko = rsAllAktywne.getString(6);
            String answerTelefon = rsAllAktywne.getString(7);
            String answerEmail = rsAllAktywne.getString(8);

            list1.addAll(ValuesSerwis.getValuesSerwis(answerNumerZgloszenia, answerDataZgloszenia, answerDecyzja, answerStatus, answerNumerSeryjny, answerNazwaUrzadzenia, answerTyp, answerCena, answerNumerKlienta, answerNazwisko, answerTelefon, answerEmail));
            numerZgloszeniaAktualne.setCellValueFactory(new PropertyValueFactory<>("numerZgloszenia"));
            dataZgloszeniaAktualne.setCellValueFactory(new PropertyValueFactory<>("data"));
            decyzjaAktualne.setCellValueFactory(new PropertyValueFactory<>("Decyzja"));
            statusAktualne.setCellValueFactory(new PropertyValueFactory<>("Status"));
            numerSeryjnyAktualne.setCellValueFactory(new PropertyValueFactory<>("numerSeryjny"));
            nazwaUrzadzeniaAktualne.setCellValueFactory(new PropertyValueFactory<>("nazwaUrzadzenia"));
            typAktualne.setCellValueFactory(new PropertyValueFactory<>("Typ"));
            cenaAktualne.setCellValueFactory(new PropertyValueFactory<>("Cena"));
            numerKlientaAktualne.setCellValueFactory(new PropertyValueFactory<>("numerKlienta"));
            nazwiskoAktualne.setCellValueFactory(new PropertyValueFactory<>("imieNazwisko"));
            telefonAktualne.setCellValueFactory(new PropertyValueFactory<>("numerTelefonu"));
            emailAktualne.setCellValueFactory(new PropertyValueFactory<>("Email"));
            table1.setItems(list1);
    }}
    /**
     * Initializing fields used in the scene
     * @throws SQLException exception
     */
    @FXML
    void initialize() throws SQLException {
        assert nazwaUrzadzeniaAktualne != null : "fx:id=\"nazwaUrzadzeniaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert typAktualne != null : "fx:id=\"typAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert btnUpdate != null : "fx:id=\"btnUpdate\" was not injected: check your FXML file 'serwis.fxml'.";
        assert nazwiskoZakonczone != null : "fx:id=\"nazwiskoZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerZgloszeniaAktualne != null : "fx:id=\"numerZgloszeniaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert table2 != null : "fx:id=\"table2\" was not injected: check your FXML file 'serwis.fxml'.";
        assert table1 != null : "fx:id=\"table1\" was not injected: check your FXML file 'serwis.fxml'.";
        assert dataZgloszeniaAktualne != null : "fx:id=\"dataZgloszeniaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert dataZakonczenia != null : "fx:id=\"dataZakonczenia\" was not injected: check your FXML file 'serwis.fxml'.";
        assert statusZakonczone != null : "fx:id=\"statusZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerKlientaZakonczone != null : "fx:id=\"numerKlientaZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerZgloszeniaZakonczone != null : "fx:id=\"numerZgloszeniaZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert typZakonczone != null : "fx:id=\"typZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert btnBack != null : "fx:id=\"btnBack\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerSeryjnnyZakonczone != null : "fx:id=\"numerSeryjnnyZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert decyzjaAktualne != null : "fx:id=\"decyzjaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert cenaAktualne != null : "fx:id=\"cenaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert cenaZakonczone != null : "fx:id=\"cenaZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerKlientaAktualne != null : "fx:id=\"numerKlientaAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert statusAktualne != null : "fx:id=\"statusAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert telefonAktualne != null : "fx:id=\"telefonAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert decyzjaZakonczone != null : "fx:id=\"decyzjaZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert telefonZakonczone != null : "fx:id=\"telefonZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert nazwaUrzadzeniaZakonczone != null : "fx:id=\"nazwaUrzadzeniaZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert nazwiskoAktualne != null : "fx:id=\"nazwiskoAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert numerSeryjnyAktualne != null : "fx:id=\"numerSeryjnyAktualne\" was not injected: check your FXML file 'serwis.fxml'.";
        assert emailZakonczone != null : "fx:id=\"emailZakonczone\" was not injected: check your FXML file 'serwis.fxml'.";
        assert emailAktualne != null : "fx:id=\"emailAktualne\" was not injected: check your FXML file 'serwis.fxml'.";


        }


    }



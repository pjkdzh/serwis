package edu.ib.bazaserwis;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Takes the user to the designated window
 * and carries the message of the field
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class StartView extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param primaryStage primary stage
     * @throws IOException exception
     * @throws SQLException exception
     */
    public void start(Stage primaryStage) throws Exception {
        Parent admin = FXMLLoader.load(getClass().getResource("/fxml/start_view.fxml"));
        Scene scene = new Scene(admin, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

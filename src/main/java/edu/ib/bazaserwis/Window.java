package edu.ib.bazaserwis;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Create a new scene
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */

public class Window {
    private final Parent admin;
    private final int width;
    private final int height;

    public Window(Parent admin, int width, int height) {
        this.admin = admin;
        this.width = width;
        this.height = height;
    }

    /**
     * Create a new scene
     * @param event event
     * @throws IOException exception
     */
    public void newWindow(ActionEvent event) throws IOException {

        Scene sceneKlientLogin = new Scene(admin, width, height);
        Stage newWindow = new Stage();
        newWindow.setScene(sceneKlientLogin);
        newWindow.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

}

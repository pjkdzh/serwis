package edu.ib.bazaserwis;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
/**
 * Carries the message of the field,
 * takes the user to the designated window
 * and carries the message of the field
 * checks the value of the field against
 * the value in the database and allows access
 * to edit and save new values of this field
 * and throws errors
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class SerwisDodajNowyController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField dataZakupu;

    @FXML
    private Button btnBack;

    @FXML
    private TextField zgloszenie;

    @FXML
    private TextField numerSeryjny;

    @FXML
    private TextField typ;

    @FXML
    private Button btnDodaj;

    @FXML
    private Text error;

    @FXML
    private TextField nazwa;

    @FXML
    private TextField nazwisko;

    @FXML
    private TextField telefon;

    @FXML
    private TextField klient;

    @FXML
    private TextField cena;

    @FXML
    private TextField email;

    @FXML
    private TextField opis;

    private String loginField;
    /**
     * Carries the message of the field
     * @param message message
     * @throws SQLException exception
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnBackClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/jaki_klient.fxml"));
        Parent root = loader.load();
        JakiKlientController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Checks the value of the field against the value in the database
     * and allows access to edit and save new values of this field
     * and throws errors
     * @param event event
     * @throws SQLException exception
     * @throws IOException exception
     */
    @FXML
    void btnDodajClicked(ActionEvent event) throws SQLException, IOException {
        PreparedStatement selectZgloszenie = Main.databaseConnection().prepareStatement("select numer_zgloszenia from zgloszenie where numer_zgloszenia = " + "\"" + zgloszenie.getText() + "\"" + ";");
        ResultSet rsZgloszenie = selectZgloszenie.executeQuery();
        String answerZgloszenie = "";
        while (rsZgloszenie.next()) {
            answerZgloszenie = rsZgloszenie.getString(1);
        }
        if(!answerZgloszenie.equals(zgloszenie.getText())) {
            PreparedStatement selectKlient = Main.databaseConnection().prepareStatement("select numer_klienta from klient where numer_klienta = " + "\"" + klient.getText() + "\"" + ";");
            ResultSet rsKlient = selectKlient.executeQuery();
            String answerKlient = "";
            while (rsKlient.next()) {
                answerKlient = rsKlient.getString(1);
            }
            if (!answerKlient.equals(klient.getText())) {
                error.setText("");
                PreparedStatement selectUrzadzenie = Main.databaseConnection().prepareStatement("select numer_seryjny from urzadzenie where numer_seryjny = " + "\"" + numerSeryjny.getText() + "\"" + ";");
                ResultSet rsUrzadzenie = selectUrzadzenie.executeQuery();
                String answerUrzadzenie = "";
                while (rsUrzadzenie.next()) {
                    answerUrzadzenie = rsUrzadzenie.getString(1);
                }
                if (!numerSeryjny.getText().equals(answerUrzadzenie)) {
                    error.setText("");
                    Statement insertKlient = Main.databaseConnection().createStatement();
                    insertKlient.executeUpdate("insert into klient(numer_klienta, imie_nazwisko, numer_telefonu, email, login, haslo) values (" +
                            "\"" + klient.getText() + "\"," + "\"" + nazwisko.getText() + "\"," + Integer.parseInt(telefon.getText()) + ","
                            + "\"" + email.getText() + "\"," + "\"" + klient.getText() + "\"," + "md5(\"" + klient.getText() + "\")" + ");");
                    Statement insertUrzadzenie = Main.databaseConnection().createStatement();
                    insertUrzadzenie.executeUpdate("insert into urzadzenie(numer_seryjny, nazwa, typ, data_zakupu, cena, numer_klienta) values (" +
                            "\"" + numerSeryjny.getText() + "\"," + "\"" + nazwa.getText() + "\"," + "\"" + typ.getText() + "\","
                            + "\"" + dataZakupu.getText() + "\"," + Double.parseDouble(cena.getText()) + ",\"" + klient.getText() + "\"" + ");");
                    Statement insertZgloszenie = Main.databaseConnection().createStatement();
                    insertZgloszenie.executeUpdate("insert into zgloszenie(numer_zgloszenia, data_zakupu, data_zgloszenia, numer_serwisu, numer_seryjny_u, opis) values (" +
                            "\"" + zgloszenie.getText() + "\"," + "\"" + dataZakupu.getText() + "\", current_date()," + "\"" + loginField + "\","
                            + "\"" + numerSeryjny.getText() + "\"," + "\"" + opis.getText() + "\"" + ");");
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/jaki_klient.fxml"));
                    Parent root = loader.load();
                    JakiKlientController scene2Controller = loader.getController();
                    scene2Controller.transferMessage(loginField);
                    new Window(root, 800, 600).newWindow(event);
                } else {
                    error.setText("Urządzenie przypisane do innego klienta");
                }

            } else {
                error.setText("Klient o danym numerze znajduje się w bazie");
            }
        }else{
            error.setText("Numer zgłoszenia znajduje się w bazie");
        }
    }

    /**
     * Initializing fields used in the scene
     */
    @FXML
    void initialize() {
        assert dataZakupu != null : "fx:id=\"dataZakupu\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert btnBack != null : "fx:id=\"btnBack\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert zgloszenie != null : "fx:id=\"zgloszenie\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert numerSeryjny != null : "fx:id=\"numerSeryjny\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert typ != null : "fx:id=\"typ\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert btnDodaj != null : "fx:id=\"btnDodaj\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert error != null : "fx:id=\"error\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert nazwa != null : "fx:id=\"nazwa\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert nazwisko != null : "fx:id=\"nazwisko\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert telefon != null : "fx:id=\"telefon\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert klient != null : "fx:id=\"klient\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert cena != null : "fx:id=\"cena\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert email != null : "fx:id=\"email\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";
        assert opis != null : "fx:id=\"opis\" was not injected: check your FXML file 'serwis_dodaj_nowy.fxml'.";

    }
}


package edu.ib.bazaserwis;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Carries the message of the field
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class ValuesFromScene {
        @FXML
        private TextField display;

    /**
     * Carries the message of the field
     * @param message message
     */
        public void transferMessage(String message) {
            display.setText(message);
        }
    }


package edu.ib.bazaserwis;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connects to the database
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class Main{
    /**
     * Connects to the database
     * @return connection
     * @throws SQLException exception
     */
    public static Connection databaseConnection() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/serwis", "admin", "admin");
        return connection;
    }
}

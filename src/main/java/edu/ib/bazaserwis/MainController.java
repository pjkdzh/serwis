package edu.ib.bazaserwis;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *  Create a method that carries responsible
 *  for logging in checks the entered login
 *  and password against the database
 *  and throws errors
 *
 *  @author Ewelina Banach, Matyna Turlik
 *  @version 1.0
 */
public class MainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text txtLogin;

    @FXML
    private Text txtError;

    @FXML
    private TextField password;

    @FXML
    private Button btnLogin;

    @FXML
    private TextField login;

    /**
     *  Method responsible for logging in
     *  checks the entered login and password against
     *  the database and throws errors
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void loginK(ActionEvent event) throws IOException {
        final String loginFiled = login.getText();
        String hasloFiled = password.getText();
        try {

            PreparedStatement selectLogin = Main.databaseConnection().prepareStatement("select login from dla_klienta where login = " + "\"" + loginFiled + "\"" + ";");
            ResultSet rsLogin = selectLogin.executeQuery();

            String answerLogin = "";
            while (rsLogin.next()) {
                answerLogin = rsLogin.getString(1);
            }
            if (loginFiled.equals(answerLogin)) {
                PreparedStatement selectHaslo = Main.databaseConnection().prepareStatement("select login from dla_klienta where haslo = " + "md5(\"" + hasloFiled + "\")" + ";");
                ResultSet rsHaslo = selectHaslo.executeQuery();

                String answerHaslo = "";
                while (rsHaslo.next()) {
                    answerHaslo = rsHaslo.getString(1);
                }
                if (hasloFiled.equals(answerHaslo)) {
                    txtError.setText("");

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/klient.fxml"));
                    Parent root = loader.load();
                    KlientController scene2Controller = loader.getController();
                    scene2Controller.transferMessage(loginFiled);
                    new Window(root,800, 600).newWindow(event);
                } else {
                    txtError.setText("Błędne dane");
                }
            } else {
                txtError.setText("Błędne dane");
            }

        } catch (SQLException e) {
        }
    }

    /**
     * Takes the user to the designated window
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void btnBackToStartClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/start_view.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }

    /**
     * Initializing fields used in the scene
     */
    @FXML
    void initialize() {
        assert txtLogin != null : "fx:id=\"txtLogin\" was not injected: check your FXML file 'main.fxml'.";
        assert txtError != null : "fx:id=\"txtError\" was not injected: check your FXML file 'main.fxml'.";
        assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'main.fxml'.";
        assert btnLogin != null : "fx:id=\"btnLogin\" was not injected: check your FXML file 'main.fxml'.";
        assert login != null : "fx:id=\"login\" was not injected: check your FXML file 'main.fxml'.";


    }
}



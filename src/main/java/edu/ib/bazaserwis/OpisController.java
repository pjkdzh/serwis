package edu.ib.bazaserwis;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;

/**
 * Create a method that carries the message of the field,
 * sends the text field value from another scene
 * and uses it, takes the user to the designated window
 * and carries the message of the field, select the
 * value of a field based on the corresponding login
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class OpisController {

    private ObservableList<String> number = FXCollections.observableArrayList("");

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox zgloszenieOpis;

    @FXML
    private Text textOpis;

    private String loginField;

    /**
     * Carries the message of the field
     * @param message message
     * @throws SQLException exception
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
        init(loginField);
    }

    /**
     * Sends the text field value from another scene
     * and uses it
     * @param event event
     * @throws SQLException exception
     */
    @FXML
    void zgloszenieOpisClicked(ActionEvent event) throws SQLException {
        PreparedStatement selectOpis = Main.databaseConnection().prepareStatement("select opis from dla_serwisu_aktywne where numerZgloszenia = " + "\"" + zgloszenieOpis.getValue() + "\" and login = " +"\"" + loginField + "\";");
        ResultSet opis = selectOpis.executeQuery();
        while (opis.next()) {
            String answerOpis = opis.getString(1);
            textOpis.setText(answerOpis);
        }
    }

    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnBackClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis.fxml"));
        Parent root = loader.load();
        SerwisController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 1400, 600).newWindow(event);
    }

    /**
     * select the value of a field based on the corresponding login
     * @param loginField login
     * @throws SQLException exception
     */
    @FXML
    void init(String loginField) throws SQLException {
        PreparedStatement selectZgloszenieAktywne = Main.databaseConnection().prepareStatement("select numerZgloszenia from dla_serwisu_aktywne where login = " + "\"" + loginField + "\"" + ";");
        ResultSet zgloszenieAktywne = selectZgloszenieAktywne.executeQuery();
        while (zgloszenieAktywne.next()) {
            String answerZgloszenie = zgloszenieAktywne.getString(1);
            number.addAll(answerZgloszenie);
        }
        zgloszenieOpis.setItems(number);
    }

    /**
     * Initializing fields used in the scene
     * @throws SQLException exception
     */
    @FXML
    void initialize() throws SQLException {
        assert zgloszenieOpis != null : "fx:id=\"zgloszenieOpis\" was not injected: check your FXML file 'opis.fxml'.";
        assert textOpis != null : "fx:id=\"textOpis\" was not injected: check your FXML file 'opis.fxml'.";

    }
}


package edu.ib.bazaserwis;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;

/**
 * Create a method that takes the user to
 * the designated window
 * and carries the message of the field
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class JakiKlientController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnStaryKlient;

    @FXML
    private Button btnNowyKlient;

    private String loginField;
    /**
     * Carries the message of the field
     * @param message e
     * @throws SQLException e
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnNowyKlientClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis_dodaj_nowy.fxml"));
        Parent root = loader.load();
        SerwisDodajNowyController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root,800, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnStaryKlientClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis_dodaj.fxml"));
        Parent root = loader.load();
        SerwisDodajController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root,800, 600).newWindow(event);
    }

    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnBackClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis_wybor.fxml"));
        Parent root = loader.load();
        SerwisWyborController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Initializing fields used in the scene
     */
    @FXML
    void initialize() {
        assert btnStaryKlient != null : "fx:id=\"btnStaryKlient\" was not injected: check your FXML file 'jaki_klient.fxml'.";
        assert btnNowyKlient != null : "fx:id=\"btnNowyKlient\" was not injected: check your FXML file 'jaki_klient.fxml'.";

    }
}


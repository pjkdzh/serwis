package edu.ib.bazaserwis;

import java.util.ArrayList;
/**
 * Creates a class and
 * adds values to the list
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class ValuesSerwis {
    private String numerZgloszenia;
    private String data;
    private String decyzja;
    private String status;
    private String numerSeryjny;
    private String nazwaUrzadzenia;
    private String typ;
    private String cena;
    private String numerKlienta;
    private String imieNazwisko;
    private String numerTelefonu;
    private String email;

    /**
     * Creates a class
     * @param numerZgloszenia numerZgłoszenia
     * @param data data
     * @param decyzja decyzja
     * @param status status
     * @param numerSeryjny numerSeryjny
     * @param nazwaUrzadzenia nazwaUrządzenia
     * @param typ typ
     * @param cena cena
     * @param numerKlienta numerKlienta
     * @param imieNazwisko imieNazwisko
     * @param numerTelefonu numerTelefonu
     * @param email email
     */
    public ValuesSerwis(String numerZgloszenia, String data, String decyzja, String status, String numerSeryjny, String nazwaUrzadzenia, String typ, String cena, String numerKlienta, String imieNazwisko, String numerTelefonu, String email) {
        this.numerZgloszenia = numerZgloszenia;
        this.data = data;
        this.decyzja = decyzja;
        this.status = status;
        this.numerSeryjny = numerSeryjny;
        this.nazwaUrzadzenia = nazwaUrzadzenia;
        this.typ = typ;
        this.cena = cena;
        this.numerKlienta = numerKlienta;
        this.imieNazwisko = imieNazwisko;
        this.numerTelefonu = numerTelefonu;
        this.email = email;
    }

    public String getNumerZgloszenia() {
        return numerZgloszenia;
    }

    public void setNumerZgloszenia(String numerZgloszenia) {
        this.numerZgloszenia = numerZgloszenia;
    }

    public String getData() {
        return data;
    }

    public void setData(String dataZgloszenia) {
        this.data = dataZgloszenia;
    }

    public String getDecyzja() {
        return decyzja;
    }

    public void setDecyzja(String decyzja) {
        this.decyzja = decyzja;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumerKlienta() {
        return numerKlienta;
    }

    public void setNumerKlienta(String numerKlienta) {
        this.numerKlienta = numerKlienta;
    }

    public String getImieNazwisko() {
        return imieNazwisko;
    }

    public void setImieNazwisko(String imieNazwisko) {
        this.imieNazwisko = imieNazwisko;
    }

    public String getNumerTelefonu() {
        return numerTelefonu;
    }

    public void setNumerTelefonu(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumerSeryjny() {
        return numerSeryjny;
    }

    public void setNumerSeryjny(String numerSeryjny) {
        this.numerSeryjny = numerSeryjny;
    }

    public String getNazwaUrzadzenia() {
        return nazwaUrzadzenia;
    }

    public void setNazwaUrzadzenia(String nazwaUrzadzenia) {
        this.nazwaUrzadzenia = nazwaUrzadzenia;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    /**
     * adds values to the list
     * @param numerZgloszenia numerZgłoszenia
     * @param data data
     * @param decyzja decyzja
     * @param status status
     * @param numerSeryjny numerSeryjny
     * @param nazwaUrzadzenia nazwaUrządzenia
     * @param typ typ
     * @param cena cena
     * @param numerKlienta numerKlienta
     * @param imieNazwisko imieNazwisko
     * @param numerTelefonu numerTelefonu
     * @param email email
     * @return values
     */
    public static ArrayList<ValuesSerwis> getValuesSerwis(String numerZgloszenia, String data, String decyzja, String status, String numerSeryjny, String nazwaUrzadzenia, String typ, String cena, String numerKlienta, String imieNazwisko, String numerTelefonu, String email ){
        ArrayList<ValuesSerwis> values = new ArrayList<>();
        values.add(new ValuesSerwis(numerZgloszenia, data, decyzja, status, numerSeryjny, nazwaUrzadzenia, typ, cena, numerKlienta, imieNazwisko, numerTelefonu, email));
        return values;
    }
}

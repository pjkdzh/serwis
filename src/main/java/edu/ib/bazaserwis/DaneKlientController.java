package edu.ib.bazaserwis;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TextField;

/**
 * Create a method that changes the value of the field
 * has been checked against the database
 * and takes the user to the designated window
 *
 *  @author Ewelina Banach, Matyna Turlik
 *  @version 1.0
 */
public class DaneKlientController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField tfTelefon;

    @FXML
    private TextField tfLogin;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfHaslo;

    private String numerKlienta;

    /**
     * Carries the message of the field
     * @param message e
     * @throws SQLException e
     */
    public void transferMessage(String message) throws SQLException {
       numerKlienta = message;
    }

    /**
     * Method that changes the value of the field
     * after it has been checked against the database
     * @param event event
     * @throws SQLException exception
     */
    @FXML
    void numberChanged(ActionEvent event) throws SQLException {
        int nowyNumer = Integer.parseInt(tfTelefon.getText());
        Statement statement = Main.databaseConnection().createStatement();
        statement.executeUpdate("update dla_klienta set numerTelefonu = " + nowyNumer + " where numerKlienta = \"" + numerKlienta + "\";" );

        }
    /**
     * Method that changes the value of the field
     * after it has been checked against the database
     * @param event event
     * @throws SQLException exception
     */
    @FXML
    void emailChanged(ActionEvent event) throws SQLException{
        String nowyEmail = tfEmail.getText();
        Statement statement = Main.databaseConnection().createStatement();
        statement.executeUpdate("update dla_klienta set email = \"" + nowyEmail + "\" where numerKlienta = \"" + numerKlienta + "\";" );
    }
    /**
     * Method that changes the value of the field
     * after it has been checked against the database
     * @param event event
     * @throws SQLException exception
     */
    @FXML
    void passwordChanged(ActionEvent event) throws SQLException {
        String noweHaslo = tfHaslo.getText();
        Statement statement = Main.databaseConnection().createStatement();
        statement.executeUpdate("update dla_klienta set haslo = " + " md5(\"" + noweHaslo + "\")" + "where numerKlienta = \"" + numerKlienta + "\";" );
    }
    /**
     * Method that changes the value of the field
     * after it has been checked against the database
     * @param event event
     * @throws SQLException exception
     */
    @FXML
    void loginChanged(ActionEvent event) throws SQLException {
        String nowyLogin = tfLogin.getText();
        Statement statement = Main.databaseConnection().createStatement();
        statement.executeUpdate("update dla_klienta set login = \"" + nowyLogin + "\" where numerKlienta = \"" + numerKlienta + "\";");
    }

    /**
     * Takes the user to the designated window
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void onClickedBack(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Initializing fields used in the scene
     * @throws SQLException exception
     */
    @FXML
    void initialize() throws SQLException {
        assert tfTelefon != null : "fx:id=\"tfTelefon\" was not injected: check your FXML file 'dane_klient.fxml'.";
        assert tfLogin != null : "fx:id=\"tfLogin\" was not injected: check your FXML file 'dane_klient.fxml'.";
        assert tfEmail != null : "fx:id=\"tfEmail\" was not injected: check your FXML file 'dane_klient.fxml'.";
        assert tfHaslo != null : "fx:id=\"tfHaslo\" was not injected: check your FXML file 'dane_klient.fxml'.";

        }
    }



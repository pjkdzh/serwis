package edu.ib.bazaserwis;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;

import java.io.IOException;
import java.sql.SQLException;

/**
 * takes the user to the designated window
 * and carries the message of the field
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class SerwisWyborController {

    @FXML
    private Button btnDodajReklamacje;

    @FXML
    private Button btnPogdladReklamacji;

    private String loginField;
    /**
     * Carries the message of the field
     * @param message message
     * @throws SQLException exception
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException ioexception
     * @throws SQLException sqlexception
     */
    @FXML
    void onClickedDodajReklamacje(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/jaki_klient.fxml"));
        Parent root = loader.load();
        JakiKlientController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 800, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     *  @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void onClickedPodgladReklamcji(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/serwis.fxml"));
        Parent root = loader.load();
        SerwisController scene2Controller = loader.getController();
        scene2Controller.transferMessage(loginField);
        new Window(root, 1400, 600).newWindow(event);
    }
    /**
     * Takes the user to the designated window
     * and carries the message of the field
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void btnLogoutSerwisClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main_serwis.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }

}

package edu.ib.bazaserwis;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Create a method that carries the message of the field,
 * takes the user to the designated window
 * carries the message of the text field,
 * takes the user to the designated window
 * using data from a view created in a database
 * and using them in the table in scene
 *
 * @author Ewelina Banach, Matyna Turlik
 * @version 1.0
 */
public class KlientController {

    private ObservableList<Values> list = FXCollections.observableArrayList();
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableColumn<Values, String> zgloszenie;

    @FXML
    private Text txtTelefon;

    @FXML
    private TableColumn<Values, String> data;

    @FXML
    private Text txtKlient;

    @FXML
    private Button btnDane;

    @FXML
    private Text txtNazwisko;

    @FXML
    private TableColumn<Values, String> serwis;

    @FXML
    private Text txtEmail;

    @FXML
    private TableColumn<Values, String> decyzja;

    @FXML
    private TableColumn<Values, String> status;

    @FXML
    private TableView<Values> table;

    private String loginField;

    /**
     * Carries the message of the field
     * @param message message
     * @throws SQLException exception
     */
    public void transferMessage(String message) throws SQLException {
        this.loginField = message;
        init(loginField);
    }

    /**
     * Takes the user to the designated window
     * and carries the message of the text field
     * @param event event
     * @throws IOException exception
     * @throws SQLException exception
     */
    @FXML
    void btnDaneClicked(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/dane_klient.fxml"));
        Parent root = loader.load();
        DaneKlientController scene = loader.getController();
        scene.transferMessage(txtKlient.getText());
        new Window(root, 800, 600).newWindow(event);
    }

    /**
     * Takes the user to the designated window
     * @param event event
     * @throws IOException exception
     */
    @FXML
    void btnLogoutClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        Parent root = loader.load();
        new Window(root, 800, 600).newWindow(event);
    }

    /**
     * Using data from a view created in a database
     * and using them in the table in scene
     * @param loginField login
     * @throws SQLException exception
     */
    @FXML
    void init(String loginField) throws SQLException {
        PreparedStatement selectAll = Main.databaseConnection().prepareStatement("select * from dla_klienta where login = " + "\"" + loginField + "\"" + ";");
        ResultSet rsAll = selectAll.executeQuery();
        while (rsAll.next()) {
            String answerNumerKlienta = rsAll.getString(1);
            String answerImieNazwisko = rsAll.getString(2);
            String answerNumerTelefonu = rsAll.getString(5);
            String answerEmail = rsAll.getString(6);
            String answerNazwaSerwisu = rsAll.getString(7);
            String answerNumerZgloszenia = rsAll.getString(8);
            String answerDataZgloszenia = rsAll.getString(9);
            String answerStatus = rsAll.getString(10);
            String answerDecyzja = rsAll.getString(11);

            list.addAll(Values.getValues(answerNazwaSerwisu, answerNumerZgloszenia, answerDataZgloszenia, answerStatus, answerDecyzja));
            txtKlient.setText(answerNumerKlienta);
            txtNazwisko.setText(answerImieNazwisko);
            txtEmail.setText(answerEmail);
            txtTelefon.setText(answerNumerTelefonu);
            serwis.setCellValueFactory(new PropertyValueFactory<>("Serwis"));
            zgloszenie.setCellValueFactory(new PropertyValueFactory<>("Zgloszenie"));
            data.setCellValueFactory(new PropertyValueFactory<>("Data"));
            status.setCellValueFactory(new PropertyValueFactory<>("Status"));
            decyzja.setCellValueFactory(new PropertyValueFactory<>("Decyzja"));
            table.setItems(list);
        }
    }

    /**
     * Initializing fields used in the scene
     * @throws SQLException exception
     */
    @FXML
    void initialize() throws SQLException {
        assert zgloszenie != null : "fx:id=\"zgloszenie\" was not injected: check your FXML file 'klient.fxml'.";
        assert txtTelefon != null : "fx:id=\"txtTelefon\" was not injected: check your FXML file 'klient.fxml'.";
        assert data != null : "fx:id=\"data\" was not injected: check your FXML file 'klient.fxml'.";
        assert txtKlient != null : "fx:id=\"txtKlient\" was not injected: check your FXML file 'klient.fxml'.";
        assert btnDane != null : "fx:id=\"btnDane\" was not injected: check your FXML file 'klient.fxml'.";
        assert txtNazwisko != null : "fx:id=\"txtNazwisko\" was not injected: check your FXML file 'klient.fxml'.";
        assert serwis != null : "fx:id=\"serwis\" was not injected: check your FXML file 'klient.fxml'.";
        assert txtEmail != null : "fx:id=\"txtEmail\" was not injected: check your FXML file 'klient.fxml'.";
        assert table != null : "fx:id=\"table\" was not injected: check your FXML file 'klient.fxml'.";
        assert decyzja != null : "fx:id=\"decyzja\" was not injected: check your FXML file 'klient.fxml'.";
        assert status != null : "fx:id=\"status\" was not injected: check your FXML file 'klient.fxml'.";
        }
    }



drop database serwis;
create database serwis;
use serwis;

create table klient(
numer_klienta char(5) primary key,
imie_nazwisko varchar(90),
numer_telefonu decimal(9,0),
email varchar(90),
login varchar(40) unique,
haslo varchar(40) unique
);

insert into klient (numer_klienta, imie_nazwisko, numer_telefonu, email, login, haslo) values
("K65", "Jan Kowalski", 789456123, "kno@gmail.com", "K65",md5("K65")),
("K66", "Anna Nowak", 789456122, "oko@gmail.com", "K66", md5("K66")),
("K67", "Dorota Kwiatek", 888456123, "okno@gmail.com", "K67", md5("K67")),
("K68", "Renata Sosna", 999555411, "oo@gmail.com", "K68", md5("K68"));

select * from klient;
select * from klient where haslo = md5("K67");

drop table serwis;
create table serwis(
numer_serwisu char(5) primary key,
nazwa_serwisu varchar(50),
login varchar(50) unique,
haslo varchar(50) unique
);

insert into serwis(numer_serwisu, nazwa_serwisu, login, haslo) values
("S56", "Aldonkahah", "S56", md5("S56")),
("S57", "Zenekhah","S57", md5("S57")),
("S58", "Reniahah","S58", md5("S58")),
("S59", "Waldekhah","S59", md5("S59"));

drop table urzadzenie;
create table urzadzenie(
numer_seryjny char(5) primary key,
nazwa varchar(45),
typ varchar(45),
data_zakupu date,
cena decimal(7,2),
numer_klienta char(5),
FOREIGN KEY (numer_klienta) REFERENCES klient(numer_klienta)
);

insert into urzadzenie(numer_seryjny, nazwa, typ, data_zakupu, cena, numer_klienta) values
("U56", "Sony", "telefon", "2021-06-14", 754, "K65"),
("U57", "Samsung", "tablet", "2020-06-14", 841, "K66"),
("U58", "Sony", "televizor", "2021-03-14", 2345.99, "K67"),
("U59", "Kuawei", "telefon", "2014-06-14", 1411, "K68");
insert into urzadzenie(numer_seryjny, nazwa, typ, data_zakupu, cena, numer_klienta) values
("U60", "Huawei", "telefon", "2014-06-14", 1411, "K68");


select * from urzadzenie;

drop table zgloszenie;
create table zgloszenie(
numer_zgloszenia char(5) primary key,
data_zakupu date,
data_zgloszenia date,
numer_serwisu char(5),
numer_seryjny_u char(5),
foreign key (numer_serwisu) references serwis(numer_serwisu),
foreign key (numer_seryjny_u) references urzadzenie(numer_seryjny),
opis varchar(200)
);

insert into zgloszenie(numer_zgloszenia, data_zakupu, data_zgloszenia, numer_serwisu, numer_seryjny_u, opis) values
("Z87", "2021-06-14", "2021-09-14", "S56", "U56", "zepsuty wyświetlacz"),
("Z88", "2020-06-14", "2021-09-14", "S57", "U57", "nie chce się włączyć"),
("Z89", "2021-03-14", "2021-09-14", "S58", "U58", "brak głosu"),
("Z90", "2014-06-14", "2021-09-14", "S59", "U59", "pęknięty ekran"),
("Z91", "2015-06-14", "2021-09-14", "S58", "U59", "zepsuty wyświetlacz"),
("Z92", "2021-06-14", "2021-09-14", "S58", "U59","zepsuty wyświetlacz");

select * from zgloszenie;

drop table zgloszenie_zakonczone;
create table zgloszenie_zakonczone(
numer_zgloszenia char(5) primary key,
data_zakonczenia date,
foreign key (numer_zgloszenia) references zgloszenie(numer_zgloszenia)
);
select * from zgloszenie_zakonczone;

drop table decyzja;
create table decyzja(
decyzja enum("","Naprawa", "Nowe urzadzenie", "Odrzucono"),
status enum("","Rozpatrzone", "W trakcie", "Nierozpatrzone"),
numer_zgloszenia char(5),
foreign key (numer_zgloszenia) references zgloszenie(numer_zgloszenia)
);
select * from decyzja;

-- funkcje------------------------------------------------------

drop function gwarancja;
DELIMITER //
	CREATE FUNCTION gwarancja (data_zakupu date, data_zgloszenia date)
		RETURNS boolean 
		DETERMINISTIC
		BEGIN
			DECLARE trwanie_gwarancji boolean;
            declare dni_zakonczenie int;
            set dni_zakonczenie = datediff(date_add(data_zakupu, interval 2 year), data_zakupu);
			if datediff(data_zgloszenia, data_zakupu) <= dni_zakonczenie then
				set trwanie_gwarancji = true;
			else
				set trwanie_gwarancji = false;
			end if;
            return trwanie_gwarancji;
	END; //
DELIMITER ;
    
-- wyzwalacze -------------------------------------------------------

drop trigger after_zgloszenie_decyzja;
delimiter $$
create trigger after_zgloszenie_decyzja
after insert
on zgloszenie for each row
begin
if gwarancja(new.data_zakupu, new.data_zgloszenia) = 0 then
	insert into decyzja(decyzja, status, numer_zgloszenia)
    values ("Odrzucono", "Rozpatrzone", new.numer_zgloszenia);
    insert into zgloszenie_zakonczone(numer_zgloszenia, data_zakonczenia)
    values (new.numer_zgloszenia, current_date());
else
	insert into decyzja(decyzja, status, numer_zgloszenia)
    values ("", "W trakcie", new.numer_zgloszenia);
    end if;
end $$
delimiter ;

drop trigger after_rozpatrzone;
delimiter $$
create trigger after_rozpatrzone
after update
on decyzja for each row
begin
	if new.status = "Rozpatrzone" then 
    insert into zgloszenie_zakonczone(numer_zgloszenia, data_zakonczenia)
    values (new.numer_zgloszenia, current_date());
    end if;
end $$
delimiter ;

insert into zgloszenie(numer_zgloszenia, data_zakupu, data_zgloszenia, numer_serwisu, numer_seryjny_u) 
values ("Z38", "2018-12-01", current_date(), "S56", "U56");
update decyzja set status = "Rozpatrzone" where numer_zgloszenia = "Z88";


-- widoki ----------------------------------------------------------------------------------------

drop view dla_klienta;
create view dla_klienta as
select k.numer_klienta as "numerKlienta", k.imie_nazwisko  as "imieNazwisko", k.haslo as "haslo", k.login as "login", k.numer_telefonu as "numerTelefonu", 
k.email as "email", s.nazwa_serwisu as "nazwaSerwisu", z.numer_zgloszenia as "numerZgloszenia", z.data_zgloszenia as "dataZgloszenia",
d.status as "status", d.decyzja as "decyzja" from klient k join serwis s join urzadzenie u join zgloszenie z join decyzja d 
on k.numer_klienta = u.numer_klienta and z.numer_serwisu = s.numer_serwisu and u.numer_seryjny = z.numer_seryjny_u and 
z.numer_zgloszenia = d.numer_zgloszenia;

drop view dla_serwisu_aktywne;
create view dla_serwisu_aktywne as
select z.numer_zgloszenia as "numerZgloszenia", z.data_zgloszenia as "datazgloszenia", d.decyzja as "decyzja", d.status as "status",
k.numer_klienta as "numerKlienta", k.imie_nazwisko as "imieNazwisko", k.numer_telefonu as "numerTelefonu", 
k.email as "email", u.numer_seryjny as "numerSeryjny", u.nazwa as "nazwaUrzadzenia", u.typ as "typUrzadzenia", u.cena as "cena", z.opis as "opis",
s.login as "login" from klient k join serwis s join urzadzenie u join zgloszenie z join decyzja d 
on k.numer_klienta = u.numer_klienta and z.numer_serwisu = s.numer_serwisu and u.numer_seryjny = z.numer_seryjny_u and z.numer_zgloszenia = d.numer_zgloszenia where status != "Rozpatrzone" ;

drop view dla_serwisu_zakonczone;
create view dla_serwisu_zakonczone as
select z.numer_zgloszenia as "numerZgloszenia", z.data_zgloszenia as "datazgloszenia", d.decyzja as "decyzja", d.status as "status",
zz.data_zakonczenia as "dataZakonczenia", k.numer_klienta as "numerKlienta", k.imie_nazwisko as "imieNazwisko", k.numer_telefonu as "numerTelefonu", 
k.email as "email", u.numer_seryjny as "numerSeryjny", u.nazwa as "nazwaUrzadzenia", u.typ as "typUrzadzenia", u.cena as "cena", 
s.login  as "login"  from klient k join serwis s join urzadzenie u join zgloszenie z join decyzja d join zgloszenie_zakonczone zz
on k.numer_klienta = u.numer_klienta and z.numer_serwisu = s.numer_serwisu and u.numer_seryjny = z.numer_seryjny_u and z.numer_zgloszenia = d.numer_zgloszenia 
and z.numer_zgloszenia = zz.numer_zgloszenia;

select * from dla_klienta;
select * from dla_serwisu_zakonczone;
select * from dla_serwisu_aktywne;

select status from dla_serwisu where numerZgloszenia = "Z87";
update dla_serwisu_aktywne set status = "Rozpatrzone" where numerZgloszenia = "Z89";
update dla_serwisu set decyzja = "Naprawa" where numerZgloszenia = "Z89";


select * from dla_klienta where login = "K65";

-- uzytkownicy -------------------------------------------------------------------------------------------

drop user "serwis"@"localhost";
create user "serwis"@"localhost" identified by "serwis";
grant select, insert, update on serwis.dla_serwisu_zakonczone to "serwis"@"localhost";
grant select, insert, update on serwis.dla_serwisu_aktywne to "serwis"@"localhost";

drop user "klient"@"localhost";
create user "klient"@"localhost" identified by "klient";
grant select on serwis.dla_klienta to "klient"@"localhost";
grant update, insert (imieNazwisko, email, numerTelefonu, haslo, login) on serwis.dla_klienta to "klient"@"localhost";

create user "admin"@"localhost" identified by "admin";
grant all privileges on serwis.* to "admin"@"localhost";

show grants for "K65"@"localhost";
select * from mysql.user;
